const request = require('request-promise');
const cheerio = require('cheerio');
const dataStorage = require('./data.json');
const fs = require('fs');


const URL = 'https://www.imdb.com/title/tt6723592/?ref_=hm_fanfav_tt_1_pd_fp1';

(async () => {
    const response = await request(URL);

    let $ = cheerio.load(response);
    let title = $('div[class="title_wrapper"] > h1').text();
    let rating = $('span[itemprop="ratingValue"]').text();
    console.log(title, rating);

    const movie = {
        "Title": title,
        "Rating": rating
    };

    const data = JSON.stringify(movie);
    fs.writeFile('data.json', data, (err) => {
        if (err){
            throw err;
        }
        console.log("JSON data is saved");
    });

})();